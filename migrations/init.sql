use neo;

CREATE TABLE IF NOT EXISTS `user` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    `first_name` varchar(255) NOT NULL,
    `last_name` varchar(255) NOT NULL,
    `nick` varchar(255) NOT NULL,
    `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    UNIQUE KEY `idx_user_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO neo.`user` (username,password,first_name,last_name,nick) VALUES
    ('admin','$2y$10$3BBtoKWhZvTn3TVn10LiGu2LxhhBQB71viel9V8.RKPL97u7sg7Ee','AdminFN','AdminLN','AdminNick');
# plainPassword: admin

INSERT IGNORE INTO neo.`user` (username,password,first_name,last_name,nick) VALUES
    ('teszt','$2y$10$tfwx2AUsPRwxtM.hKvz3AeXvFBZRQ6tfmc8OmbagCleTupft7aHXi','TesztFN','TesztLN','TesztNick');
# plainPassword: teszt