<?php
const SRC_DIR = __DIR__ . '/../src/';
const CONFIG_DIR = __DIR__ . '/../config/';
require_once '_autoload.php';

use App\Core\Request;
use App\Core\Router;

$request = new Request();
(new Router($request))->route();
