<?php
spl_autoload_register(function ($class) {
	$class = preg_replace('/^App/', '', $class);
	$class = str_replace("\\", DIRECTORY_SEPARATOR, $class);
	if (!is_file(SRC_DIR . $class . '.php')) {
		throw new LogicException ("Class $class not found");
	}
	require_once(SRC_DIR . $class . '.php');
});
