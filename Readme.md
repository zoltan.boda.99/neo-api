Steps for setting up the API after checkout:
```shell
# starting up containers, run from project dir
docker-compose up -d
```

Preparing for the case that port `3306` is taken for local MySQL service the db container's host port is set to `3307` so in order to access it via a db management client you should use `localhost:3307` or reconfigure `services.mysql.port` in `docker-compose.yml` 

DB access details (as configured in said yaml file):
- username: `neo`
- password: `secret`
- database: `neo`

During db container startup automatic migration (structure and data) takes place if necessary.

Initially migrated users' credentials (username/password):
- admin/admin
- teszt/teszt

After all being up and running the API will be accessible via the URL [http://127.0.0.1](http://127.0.0.1) or [http://localhost](http://localhost)
You can also assign a hostname in the `hosts` file, e.g.:
```text
127.0.0.1 neo.api
```

Endpoint for getting user:
[http://127.0.0.1/user/get/1](http://127.0.0.1/user/get/1)

Endpoint for authenticating a user:
[http://127.0.0.1/user/authenticate](http://127.0.0.1/user/authenticate)

`username` and `password` values are expected in POST fields
