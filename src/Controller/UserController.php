<?php

namespace App\Controller;

use App\Core\Authentication;
use App\Core\View;
use App\Dao\UserDao;

class UserController extends AbstractController
{
	const MESSAGE_USER_AUTHENTICATED = 'User authenticated';
	const MESSAGE_AUTHENTICATION_FAILED = 'Authentication failed';

	private UserDao $dao;

	public function __construct(View $view)
	{
		parent::__construct($view);
		$this->dao = new UserDao($view);
	}

	public function get(?int $id): void
	{
		$dao = new UserDao();
		$data = $dao->getUserById($id);
		$this->view->setData($data);
		$this->view->render();
	}

	public function authenticate()
	{
		$postData = $this->view->getRequest()->getPostData();
		if (
				   empty($postData['username'])
				|| empty($postData['password'])
		) {
			$this->default();
		}

		$authentication = new Authentication();
		$result = $this->dao->authenticate(
			$authentication,
			$postData['username'],
			$postData['password']
		);

		$this->view->setMessage($result
			? self::MESSAGE_USER_AUTHENTICATED
			: self::MESSAGE_AUTHENTICATION_FAILED
		);
		$this->view->setData(['success' => $result]);
		$this->view->render();
	}
}