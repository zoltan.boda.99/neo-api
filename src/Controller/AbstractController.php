<?php

namespace App\Controller;

use App\Core\View;

abstract class AbstractController
{
	protected const DATA_MESSAGE = 'Data retireved';

	public function __construct(
		protected View $view
	)
	{
		$this->view->setMessage(self::DATA_MESSAGE);
	}

	public function default(): void
	{
		$this->view->render();
	}
}