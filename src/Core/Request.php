<?php

namespace App\Core;

class Request
{
	private string $entityBody;

	private array $getData;

	private array $postData;

	private array $uriArray;

	public function __construct()
	{
		$this->entityBody = file_get_contents('php://input');
		$this->getData = $_GET;
		$this->postData = $_POST;
		$this->setUriArray();
	}

	/**
	 * @return string
	 */
	public function getEntityBody(): string
	{
		return $this->entityBody;
	}

	/**
	 * @return array
	 */
	public function getGetData(): array
	{
		return $this->getData;
	}

	/**
	 * @return array
	 */
	public function getPostData(): array
	{
		return $this->postData;
	}

	/**
	 * @return array
	 */
	public function getUriArray(): array
	{
		return $this->uriArray;
	}

	public function setUriArray(): void
	{
		$this->uriArray = explode('/', preg_replace('/^\//', '', $_SERVER['REQUEST_URI']));
	}

}