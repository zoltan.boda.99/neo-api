<?php

namespace App\Core;

use App\Controller\IndexController;
use Exception;
use ReflectionMethod;

class Router
{
	const DEFAULT_METHOD = 'default';
	const DEFAULT_CONTROLLER = 'index';

	private string $controller;

	private string $method;

	private ?string $param;

	public function __construct(
		private readonly Request $request
	)
	{
		$params = $this->request->getUriArray();
		$this->controller = $params[0] && $params[1] ? $params[0] : 'index';
		$this->method = $params[0] && $params[1] ? $params[1] : self::DEFAULT_METHOD;
		$this->param = $params[2] ?? null;
	}

	/**
	 * @return string
	 */
	public function getController(): string
	{
		return $this->controller;
	}

	/**
	 * @return string
	 */
	public function getMethod(): string
	{
		return $this->method;
	}

	/**
	 * @return string
	 */
	public function getParam(): string
	{
		return $this->param;
	}

	public function route(): void
	{
		$view = new View($this->request);
		$controllerName = ucfirst(self::DEFAULT_CONTROLLER);
		if (file_exists(SRC_DIR . '/Controller/' . ucfirst($this->getController()) . 'Controller.php')) {
			$controllerName = ucfirst($this->getController());
		}
		$controllerName = '\App\Controller\\' . $controllerName . 'Controller';
		$controller = new $controllerName($view);
		$methodName = self::DEFAULT_METHOD;
		if (method_exists($controller, strtolower($this->getMethod()))) {
			$methodName = strtolower($this->getMethod());
		}

		try {
			$method = new ReflectionMethod($controller, $methodName);
			if($method->getParameters()) {
				$controller->{$methodName}($this->getParam());
			} else {
				$controller->{$methodName}();
			}
		} catch (Exception $e) {
			(new IndexController($view))->default();
		}
	}
}