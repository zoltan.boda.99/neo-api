<?php

namespace App\Core;

class Authentication
{
	public function hashPassword($password): string
	{
		return password_hash($password, PASSWORD_BCRYPT);
	}

	public function verifyPassword($plainPassword, $hashedPassword)
	{
		return password_verify($plainPassword, $hashedPassword);
	}
}