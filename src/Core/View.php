<?php

namespace App\Core;

class View
{
	private array $data = [];

	private string $headerString = 'Content-Type: application/json; charset=utf-8';

	private string $message = '';

	public function __construct(
		private Request $request,
	)
	{}

	/**
	 * @return Request
	 */
	public function getRequest(): Request
	{
		return $this->request;
	}

	/**
	 * @param mixed $headerString
	 */
	public function setHeaderString(string $headerString): void
	{
		$this->headerString = $headerString;
	}

	/**
	 * @param string $message
	 */
	public function setMessage(string $message): void
	{
		$this->message = $message;
	}

	/**
	 * @param array|null $data
	 */
	public function setData(?array $data = null): void
	{
		$this->data = $data;
	}

	public function __toString(): string
	{
		return json_encode([
			'message' => $this->message,
			'data' => $this->data,
		]);
	}

	public function render(): void
	{
		header($this->headerString);
		die($this);
	}
}