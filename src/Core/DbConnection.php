<?php

namespace App\Core;

use PDO;

class DbConnection
{
	static private ?PDO $db = null;

	static public function getConnection(): PDO
	{
		if (null === self::$db) {
			$dbConfig = parse_ini_file(CONFIG_DIR . "/dbConfig.ini.php");
			self::$db = new PDO($dbConfig['dsn'], $dbConfig['username'], $dbConfig['password'], [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
		}

		return self::$db;
	}
}