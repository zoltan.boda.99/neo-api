<?php

namespace App\Dao;

use App\Core\Authentication;
use PDO;

class UserDao extends AbstractDao
{
	public function getUserById($id): array
	{
		$sql = "
			SELECT
				id, username, first_name, last_name, nick, created_at
			FROM
				user
			WHERE
				id = :id
		";

		$st = $this->db->prepare($sql);
		$st->bindValue('id', $id, PDO::PARAM_INT);
		$st->execute();

		return $st->fetch(PDO::FETCH_ASSOC) ?: [];
	}

	public function authenticate(Authentication $authentication, string $username, string $plainPassword)
	{
		$sql = "
			SELECT
				password
			FROM
				user
			WHERE
				username = :username
		";

		$st = $this->db->prepare($sql);
		$st->bindValue('username', $username, PDO::PARAM_STR);
		$st->execute();

		$res = $st->fetch(PDO::FETCH_ASSOC);
		$hashedPassword = $res['password'] ?? '';

		return $authentication->verifyPassword($plainPassword, $hashedPassword);
	}
}