<?php

namespace App\Dao;

use App\Core\DbConnection;
use PDO;

abstract class AbstractDao
{
	protected PDO $db;

	public function __construct()
	{
		$this->db = DbConnection::getConnection();
	}
}